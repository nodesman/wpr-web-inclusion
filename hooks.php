<?php
/*
Plugin Name: Web Inclusion WPR Template
Plugin URI: http://www.webinclusion.com
Description: Custom template for the WP Autoresponder plugin's blog post delivery broadcasts.
Version: 0.1
Author: Raj Sekharan
Author URI: http://www.nodesman.com
*/

add_filter("_wpr_blog_delivery_email_default_layout","_wiet_blog_delivery_email_template",10, 2);


function _wiet_blog_delivery_email_template($content,$post_id)
        {
    $plugindir =  dirname(__FILE__);
    $content = file_get_contents("$plugindir/template.php");
    $content = apply_filters("_wiet_email_templates",$content,$post_id);
    return $content;
}
add_action("edit_post","_wiet_validate_post",2,2);
function _wiet_validate_post($post_id, $post) {
    if ($post->post_status == 'publish')
            {
                $excerpt = $post->post_excerpt;


                if (empty($excerpt))
                    {
                    _wiet_add_notice("<!--Post:{$post->ID}-->The blog post <a href=\"{$post->guid}\">{$post->post_title}</a> does not have an excerpt. If this blog post is delivered, the entire blog post will be delivered to e-mail subscribers. <a href=\"post.php?post={$post->ID}&action=edit\">Click here to add an excerpt</a>. ");
                }
            }

            //get rid of notices if any for this blog post
            if (!empty($post->post_excerpt))
                    {
                        $notices = get_option("_wiet_notices");
                        $updatedNotice = $notices;
                        foreach ($notices as $index=>$notice )
                            {
                            if (preg_match("@<!--Post:{$post_id}-->@",$notice))
                                    {
                                unset($updatedNotice[$index]);
                            }
                        }
                        update_option("_wiet_notices", $updatedNotice);
                    }
}


add_action("admin_notices","_wiet_notices");

function _wiet_add_notice($string)
{
    $notices = get_option("_wiet_notices");
    if (!is_array($notices))
        {
        $notices = array();
        $notices[] = $string;
    }
    else
        {
        $notices[] = $string;
    }
    update_option("_wiet_notices",$notices);
}

function _wiet_notices() {
    $notices = get_option("_wiet_notices");
    
    if (count($notices) > 0 && is_array($notices)) {
    foreach ($notices as $index=>$notice)
        {
?>
<div class="error fade">
    <p style="padding: 5px;"><?php echo $notice ?>
    <a href="<?php

    $query = $_SERVER['QUERY_STRING'];
    $parts = explode("&",$query);
    $parts[] = "_wiet_dismiss=$index";
    $parts = implode("&",$parts);
    echo str_replace($_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']); ?>?<?php echo $parts ?>">Dismiss</a></p>
</div>
<?php
        }
    }
}

add_filter("_wiet_email_templates","_wiet_email_filter",10,2);
function _wiet_email_filter($content,$post_id)
        {
    $content = str_replace("%%SITEHOME%%",get_bloginfo("url"),$content);
    $post = get_post($post_id);
    //get image
    $image = get_post_meta($post_id,"_thumbnail_id");
    $featured_image="";
    if (0 != count($image))
    {
        $image = (get_post($image[0]));
        $image_url = $image->guid;
        $actual_url = get_bloginfo("url")."/?src={$image_url}&w=200&f=jpg";
        $featured_image = sprintf('<img src="%s" hspace="10" vspace="10" align="right"/>',$actual_url);
    }
    $content = str_replace("%%FEATURED_IMAGE%%",$featured_image,$content);
    if (!empty($post->post_excerpt))
        $content = str_replace("%%EXCERPT%%",$post->post_excerpt,$content);
    else
        $content = str_replace("%%EXCERPT%%",$post->post_content,$content);
    
    $content = str_replace("%%WORDPRESS_ROOT%%",get_bloginfo("url"),$content);
    
    
    $img_url = get_option("header_image_url");
    
    if (empty($img_url))
       $img_url = sprintf("%s/images/email/email-logo1.png",get_bloginfo("url"));
       
    
    $content = str_replace("%%IMAGE%%",$img_url,$content);
    
    

        $follow_html = "";
        $tweet_url = get_option("twitter_url");
        $facebook_url = get_option("facebook_url");
        $links = array();
        if (!empty($facebook_url))
        {
            $links[] = sprintf('<a href="%s">Facebook</a>',$facebook_url);            
            $content = str_replace("%%FACEBOOK_URL%%",$facebook_url,$content);
        }
        if (!empty($tweet_url))
        {
            $links[] = sprintf('<a href="%s">Twitter</a>',$tweet_url);
            $content = str_replace("%%TWITTER_URL%%",$tweet_url,$content);
        }
        $linksCombined = implode(" | ",$links);
        if (0 < count($links))
        {
            $follow_html = sprintf("Follow us on %s",$linksCombined);
        }
        $content = str_replace("%%FOLLOW_US%%",$follow_html,$content);
    
    
    
    
    //FEATURED_IMAGE
    //%%READ_MORE%%
    $content = str_replace("%%POST_URL%%",$post->guid,$content);
    return $content;
}

add_action("init","_wiet_init");

function _wiet_init()
{
    //hook on to the
     $plugindir =  dirname(__FILE__);
     $qs = $_SERVER['QUERY_STRING'];
     if (isset($_GET['src']) && (isset($_GET['w']) || isset($_GET['h'])))
        {
         header("Content-Type: image/jpg");
         require "thumb/phpThumb.php";         
         exit;
    }

    if (isset($_GET['_wiet_dismiss']))
        {
            $notices = get_option("_wiet_notices");
            $index = $_GET['_wiet_dismiss'];
            unset($notices[$index]);
            update_option("_wiet_notices",$notices);
        }

     if (isset($_GET['wit-file'])) {

       $dir = dirname(__FILE__);
       readfile("$dir/validate.js");
       exit;
     }

     
     $directory = str_replace(basename(__FILE__),"",__FILE__);
     $containingdirectory = basename($directory);
     $url = get_bloginfo("wpurl");
     add_action("load-post-new.php","_wiet_post_edit_scripts");
     add_action("load-post.php","_wiet_post_edit_scripts");
     $containingdirectory = basename(dirname(__FILE__));
     wp_register_script( "wit_validate","/".PLUGINDIR."/".$containingdirectory."/validate.js" );

    
  /*  if (isset($_GET['preview']))    {
        echo _wiet_blog_delivery_email_template("",9);
        exit;
    }*/
    add_action('admin_menu', '_wiet_menus');
}

function _wiet_menus()
{
    add_theme_page('Blog Email Settings',"Blog Email Template",'activate_plugins',"blog_email_template","_wiet_email_template_settings");
}


function _wiet_post_edit_scripts() {
  wp_enqueue_script("jquery");

  wp_enqueue_script("wit_validate");

}


function _wiet_email_template_settings()
{
    //the form
    if (isset($_POST['_wpnonce'])) {
        $nonce=$_REQUEST['_wpnonce'];
         if (!wp_verify_nonce($nonce, "_wiet_settings"))
             die("Security Check Failed!");

         $twitter_url = $_POST['twitter_url'];
         $facebook_url = $_POST['facebook_url'];
         $image_url = $_POST['header_url'];

         update_option("twitter_url", $twitter_url);
         update_option("header_image_url", $image_url);
         update_option("facebook_url", $facebook_url);

        }
        $twitter_url = get_option("twitter_url");
        $facebook_url = get_option("facebook_url");
        $header_url = get_option("header_image_url", $image_url);
        

    ?>
<div class="wrap">
    <h2>HTML Email Template Settings</h2>
    The following URLs will be used in the blog e-mail settings in all outgoing email templates delivered by the WP Autoresponder plugin below the blog post conent.
    If either or both of these fields are left empty, the corresponding link will not be inserted in outgoing blog post emails.
    <p></p>
    <form action="themes.php?page=blog_email_template" method="post">
    <?php wp_nonce_field('_wiet_settings'); ?>
    <table width="800">
        <tr>
            <td height="50" valign="top">Twitter URL: </td>
            <td valign="top"><input type="text" size="50" name="twitter_url" value="<?php echo $twitter_url ?>"/><br/>
                <small><strong>Example:</strong> http://twitter.com/#!/tomhanks</small></td>
        </tr>
        <tr>
            <td height="50" valign="top">Facebook URL: </td>
            <td><input type="text" name="facebook_url" size="50" value="<?php echo $facebook_url ?>"/><br/>
                <small><strong>Example:</strong> http://facebook.com/tomhanks</small></td>
        </tr>
        <tr>
            <td height="50" valign="top">Header Image URL: </td>
            <td><input type="text" name="header_url" size="50" value="<?php echo $header_url ?>"/><br/>
                <small><strong>Example:</strong>http://www.mywebsite.com/images/logo.png<br />
                <strong>Defaults to</strong>: <?php echo get_bloginfo("url") ?>/images/email-logo1.png<br /> if left empty.</small>
                </td>
        </tr>
        <tr>
            <td height="50"></td>
        </tr>
    </table>
        <input type="submit" value="Save Settings" class="button-primary"/>
        </form>
</div>

<?php

}
