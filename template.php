<table width="600" align="center" style="border: 1px solid #000" cellspacing="0" cellpadding="0">
    <tr>
        <td><img src="%%IMAGE%%"></td>
    </tr>
    <tr>
        <td style="background-color: #ffffff "></td>
    </tr>
    <tr>
        <td style="padding: 10px; background-color: #ffffff; font-family: Arial; line-height: 17px; font-size: 12px;">
            Dear [!name!],
            <p>
            %%FEATURED_IMAGE%%
            %%EXCERPT%%
           
            </p>
            <a href="%%POST_URL%%">Go to website for more. Click here.</a>
            <p></p>
    With Best Wishes,<br/>
            Tim, Mow, Tim & Eliza<br/>
            The Flannels Crew<br/>
            <a href="%%SITEHOME%%">%%SITEHOME%%</a><br/>
            %%FOLLOW_US%%
        </td>
    </tr>
    <tr>
        <td style="font-family:arial; font-size: 12px; padding: 10px; "><small>If you wish to stop receiving our emails, <a href="[!unsubscribe!]">click here.</a>.</small></td>
    </tr>
</table>

