
var pubButton



function _wit_validate(a) {

           if (document.getElementById("postexcerpt").style.display == "none")
           {
               alert("An excerpt has not been added to this blog post. The currently active blog post e-mail template requires that a excerpt be specified to display optimally on e-mail clients.  At the absence of a excerpt the template will publish the entire blog post to blog e-mail subscribers regardless of the per-blog post e-mail settings specified in the 'E-mail Subscriber Settings' panel. Please edit ths blog post immediately and add the excerpt if you want to use a customized excerpt message in the e-mails that will shortly be dispatched.");
               return false;
           }

           content = jQuery.trim(document.getElementById("excerpt").value);
           if (content.length == 0)
               {
                  alert("An excerpt has not been added to this blog post. The currently active blog post e-mail template requires that a excerpt be specified to display optimally on e-mail clients.  At the absence of a excerpt the template will publish the entire blog post to blog e-mail subscribers regardless of the per-blog post e-mail settings specified in the 'E-mail Subscriber Settings' panel. Please edit ths blog post immediately and add the excerpt if you want to use a customized excerpt message in the e-mails that will shortly be dispatched.");
                  return false;
               }
       }

jQuery(document).ready(function () {

window.setTimeout(function() {
   pubButton = document.getElementById("publish");
   pubButton.setAttribute("onsubmit","return _wit_validate()");
      pubButton.addEventListener("submit", _wit_validate, true)
      pubButton.addEventListener("click", _wit_validate, true)
},1000);

});


